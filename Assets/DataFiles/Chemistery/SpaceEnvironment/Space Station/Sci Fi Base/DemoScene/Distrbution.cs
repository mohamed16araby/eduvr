using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Distrbution : MonoBehaviour
{
    public int id;
    public int finalid;
    public int max;
    public TextMeshProUGUI text;
    public Transform[] ofsset;
    public GameObject energLevel;
    public GameObject main;
    public Collider[] next;
    public Collider[] WrongCollider;


    public int snapeZoneCount = 0;
    private new Collider[] collider;
    
    // Start is called before the first frame update
    void Start()
    {
        collider = GetComponents<Collider>();
    }
    private bool firstShot = true;

    // Update is called once per frame
    void Update()
    {
        if(snapeZoneCount >= max && id != finalid) // && id != finalid
        {
            if (firstShot)
            {
                setColliders(collider, false);
                setColliders(WrongCollider, true);
                setColliders(next, true);
                firstShot = false;
                Debug.Log("000000000000000000000000  " + gameObject.name);
            }
           
        }
        else if(snapeZoneCount >= max && id == finalid)
        {
            if(firstShot)
            {
                setColliders(collider, false);
                setColliders(WrongCollider, true);
                firstShot= false;
                Debug.Log("-----------------------------  " + gameObject.name);
            }
           
        }

    }

    
    public void IncreaseSnapZoneCount()
    {
        if (snapeZoneCount >= max)
            return;

        snapeZoneCount++;
        text.text = snapeZoneCount.ToString();
    }

    public void setColliders(Collider[] colliders , bool flag)
    {
        foreach (var collider in colliders)
        {
            collider.enabled = flag;
        }
    }

    public void ReturnToPosition(Transform electron ,Transform parent)
    {
        electron.parent = parent;
       
    }
}
