using BNG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapZoneHighlighting : MonoBehaviour
{
    public GameObject snapZoneHighlight;
    public Material hightlightMat;
    public Material validMat;
    public Material unvalidMat;
    public SnapZone snapZone;

    private Material DefaultMat;

    // Start is called before the first frame update
    void Start()
    {
        DefaultMat = snapZoneHighlight.GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void HightLightHandeler()
    {

    }


    public bool IsTargetNear(bool matchFound)
    {
        Vector3 dir = snapZone.gameObject.transform.position - snapZone.getClosestGrabbable().gameObject.transform.position;
        
        return Vector3.SqrMagnitude(dir) <= .1f && matchFound;
    }

    public void Highlighting()
    {
        snapZoneHighlight.GetComponent<Renderer>().material = hightlightMat;
    }

    public void ValidHighlighting()
    {
        snapZoneHighlight.GetComponent<Renderer>().material = validMat;
    }

    public void unValidHighlighting()
    {
        snapZoneHighlight.GetComponent<Renderer>().material = unvalidMat;
    }
    
    public void ReturnToDefault()
    {
        snapZoneHighlight.GetComponent<Renderer>().material = DefaultMat;
    }
}
