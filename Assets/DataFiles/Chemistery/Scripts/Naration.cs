using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Naration : MonoBehaviour
{
    public Clips[] clips;
    public AudioSource source;
    // Start is called before the first frame update
   
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayNext(int id)
    {
        source.clip = clips[id].clip;
        source.Play();
    }
    public void PlaynextDelay(int id)
    {
        StartCoroutine(nextDelay(id));
    }

    IEnumerator nextDelay(int id)
    {
        float delay = 0;
        foreach (var clip in clips)  
        {
            if (clip.id == id)
            {
               delay = clip.clip.length;
            }
        }
        yield return new WaitForSeconds(delay);
        source.clip = clips[id].clip;
        source.Play();
    }
}
[System.Serializable]
public class Clips
{
    public int id;
    public AudioClip clip;
}
