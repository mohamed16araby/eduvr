using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class ReturnToPosition : MonoBehaviour
{
    private Transform defaultPosition;
    private Transform parent;
    private Grabbable grabble;
    // Start is called before the first frame update
    void Start()
    {
        defaultPosition = transform;
        parent = transform.parent;
        grabble = GetComponent<Grabbable>();
    }

    // Update is called once per frame
    void Update()
    {
        if (grabble.BeingHeld)
            return;
        ReturnelectronToPosition(defaultPosition);
    }
    public void ReturnelectronToPosition(Transform electron)
    {
        electron.parent = parent;
        electron.localPosition =Vector3.Lerp(electron.localPosition, defaultPosition.localPosition,2);
    }


}
