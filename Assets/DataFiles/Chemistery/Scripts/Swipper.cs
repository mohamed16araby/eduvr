using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipper : MonoBehaviour
{
    public GameObject[] examples;
    public GameObject[] moleculs;

    private int Index = 0;

    private int SnapCount;

    private int swipperState = 1;
    


    public void playNext()
    {
        switch (swipperState)
        {
            case 0:
                if (Index < examples.Length)
                {
                    SnapCount = 0;
                    examples[Index].SetActive(true);
                    foreach (var example in examples)
                    {
                        if (example.name == examples[Index].name)
                            continue;
                        example.SetActive(false);
                    }
                    Index++;
                }
                else
                    Index = 0;
                break;
            case 1:
                if (Index < moleculs.Length)
                {
                    SnapCount = 0;
                    moleculs[Index].SetActive(true);
                    foreach (var example in moleculs)
                    {
                        if (example.name == moleculs[Index].name)
                            continue;
                        example.SetActive(false);
                    }
                    Index++;
                }
                else
                    Index = 0;
                break;
            default:
                break;
        }
        
       
    }

    public void SetSwipperState(int state)
    {
        swipperState = state;
        Index = 0;
        switch (swipperState)
        {
            case 1:
                foreach (var example in examples)
                    example.SetActive(false); 
                break;
            case 0:
                foreach (var molecul in moleculs)
                    molecul.SetActive(false);

                break;
            default:
                break;
        }
    }

    public void PlayPrevious()
    {
        Debug.Log("2222222222222222");
        if (Index > 1)
        {
            Debug.Log("55555555555555555555");
            --Index;
            --Index;
        }
        else
            Index = 6;
            playNext();
        
    }


    public void TriggerEndOfExample(int countToEnd)
    {
        if (countToEnd >= SnapCount)
        {
            Debug.Log("you done it ");
            return;
        }

        SnapCount++;

    }

    public void ScaleModified(Transform snapZone)
    {
        snapZone.GetChild(0).localScale = new Vector3(1,1,1);
    }
    
}

