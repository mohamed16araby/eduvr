using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistributionOfAtom : MonoBehaviour
{
    public AtomSO[] atomSOs;
    public OrbitData[] orbits;


    public Text Symbol;
    public Text mass;
    public Text Protons;


    private AtomSO ToDislay;

    // Start is called before the first frame update
    void Start()
    {
        PlayNextRandom();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var item in orbits)
        {
            if (item.isFull)
                item.orbitCollider.enabled = false;
        }
    }

    private Transform[] electrons;
    public void PlayNextRandom()
    {
        ToDislay = atomSOs[Random.Range(0, atomSOs.Length)];
        ResetBeforeNext();
        mass.text = ToDislay.mass + "";
        Symbol.text = ToDislay.symbol;
        Protons.text = ToDislay.atomic + "";
        for (int i = 0; i < ToDislay.atomEnergyLevels.Length; i++)
        {
            Debug.Log(i);
            orbits[i].orbit.gameObject.SetActive(true);
            orbits[i].isFull = false;
        }
        foreach (var orbit in orbits)
        {
            foreach (Transform child in orbit.orbit)
            {

                Destroy(child.gameObject);
            }
        }
        
    }


    public void EnergyLevelLimit(int energylevelIndex)
    {
        orbits[energylevelIndex].eneryLevel++;        
    }

    public void isEnergylevelFull()
    {
        for (int i = 0; i < ToDislay.atomEnergyLevels.Length; i++)
        {
            if (ToDislay.atomEnergyLevels[i] == orbits[i].eneryLevel)
                orbits[i].isFull = true;
        }

    }
    public void isEnergylevelFull(int index)
    {

        if (orbits[index].eneryLevel >= ToDislay.atomEnergyLevels[index]-1)
        {
            orbits[index].isFull = true;
            Debug.Log("fulllllll");
        }
           
        else
            orbits[index].eneryLevel++;
        Debug.Log("fulllllll  " + orbits[index].eneryLevel);
    }

    public void ResetBeforeNext()
    {
        foreach (var item in orbits)
        {
            item.isFull = false;
            item.orbitCollider.enabled = false;
            item.eneryLevel = 0;
        }
        for (int i = 0; i < ToDislay.atomEnergyLevels.Length; i++)
        {
            orbits[i].orbitCollider.enabled = true;
        }
    }


}


[System.Serializable]
public class OrbitData
{
    public Transform orbit;
    public SphereCollider orbitCollider;
    [HideInInspector]public int eneryLevel;
    [HideInInspector] public bool isFull = false;
}
