﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atom :MonoBehaviour
{
    public AtomSO atomSO ;
    public string atomName;
    public string atomShymbol;
    public int Mass;
    public int protons;


    //public Atom()
    //{
    //    atomName = atomSO.name;
    //    atomShymbol = atomSO.symbol;
    //    protons = atomSO.atomic;
    //}

    private void Start()
    {
        atomName = atomSO.name;
        atomShymbol = atomSO.symbol;
        protons = atomSO.atomic;
        Debug.Log(atomSO.name);
    }

    private void Update()
    {
        
    }

}