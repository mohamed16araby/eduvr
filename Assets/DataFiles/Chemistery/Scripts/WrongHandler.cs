﻿using System.Collections;
using UnityEngine;
using BNG;

public class WrongHandler : MonoBehaviour
{
    public GameObject WrongEffect;
    public Transform intialPosition;
    public Transform parent;
    public float timebeforeFirstActivate = 2f;
    public string ObjectName = "-e";

    public string[] ObjectNames = new []{"K","L","M","N","O","P","Q"};
    public string SecObjectName ;
    public string name;
    public bool Exclude = false;
    public bool EnergyLevel = false;
    // Use this for initialization
    void Start()
    {

    }
    private Transform electron;
    private float timer = 0;
    // Update is called once per frame
    void Update()
    {
        if(electron != null && elecnull())
            electron = null;

        timer += Time.deltaTime;
        ReturnToIntial();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(EnergyLevel)
        {
            foreach (var objectname in ObjectNames)
            {
                if (other.name == ObjectName)
                    return;

                else if(other.name == objectname && objectname != ObjectName)
                {
                    electron = other.transform;
                    name = other.gameObject.name;
                    StartCoroutine(WrongEffectPlay());
                }
                
                
            }
            return;
        }

        if(Exclude)
        {
            if (other.gameObject.name == ObjectName || other.gameObject.name == SecObjectName)
            {
                electron = other.transform;
                name = other.gameObject.name;
                StartCoroutine(WrongEffectPlay());
                return;
            }
        }

        if(other.gameObject.name == ObjectName)
        {
            electron = other.transform;
            name = other.gameObject.name;
            StartCoroutine(WrongEffectPlay());
        }
    }

    
    private bool elecnull()
    {
        return Vector3.Distance(electron.position, transform.position) >= .3;
    }

    IEnumerator WrongEffectPlay()
    {
        WrongEffect.SetActive(true);
        yield return new WaitForSeconds(2);
        WrongEffect.SetActive(false);

    }

    public void ReturnToIntial()
    {
        if (electron != null)
        {

            if (electron.GetComponent<Grabbable>().BeingHeld)
            {
                return;
            }

            electron.position = Vector3.Lerp(electron.position, intialPosition.position,5*Time.deltaTime);
            electron.parent = parent;
            electron.name = name;
        }
    }
}