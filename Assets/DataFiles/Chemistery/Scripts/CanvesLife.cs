using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvesLife : MonoBehaviour
{
    public float lifeTime =0;

    private float timer=0;
    private void OnEnable()
    {
        timer = 0;
    }


    // Update is called once per frame
    void Update()
    {
        if (timer > lifeTime)
            gameObject.SetActive(false);
        else
            timer += Time.deltaTime;
    }
}
