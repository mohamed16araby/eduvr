using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndDistrbute : MonoBehaviour
{

    public GameObject atom;
    public Collider[] colliders;
    public GameObject _object;
    public bool makeItWork = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (makeItWork && gameObject.transform.childCount == 0)
        {
            atom.SetActive(false);
            foreach (var collider in colliders)
            {
                collider.enabled = false;
            }
            _object.SetActive(false);
        }
    }




}
