using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MoleCuleSO", order = 2)]
public class MoleculesSO : ScriptableObject
{

    public moleculosAtoms[] atoms;
    
}

[System.Serializable]
public class moleculosAtoms
{
    public AtomSO atomic;
    public int number = 1;
}
