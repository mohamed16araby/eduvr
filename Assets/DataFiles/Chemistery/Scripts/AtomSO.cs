using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/AtomSO", order = 1)]
public class AtomSO : ScriptableObject
{

    public GameObject atomPrefab;
    public int atomic;
    public int mass;
    public string symbol;
    public float weight;
    public Vector2 period;
    public int[] atomEnergyLevels;
}
