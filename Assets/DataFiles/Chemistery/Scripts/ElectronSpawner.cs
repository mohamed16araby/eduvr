using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronSpawner : MonoBehaviour
{
    public GameObject Spawner;
    public Transform[] SpawnPositions;

    // Update is called once per frame
    void Update()
    {
      if(transform.childCount < 5)
      {
            foreach(var spawnposition in SpawnPositions)
            {
                GameObject spawnObject = Instantiate(Spawner, transform, false);
                spawnObject.transform.SetParent(transform, false);
                spawnObject.transform.localPosition = spawnposition.localPosition;
                spawnObject.transform.localRotation = spawnposition.localRotation;
            }
      } 
    }
}
