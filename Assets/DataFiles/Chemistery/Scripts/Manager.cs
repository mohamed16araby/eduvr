using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Manager : MonoBehaviour
{
    public GameObject instruction,video,moleculesProcess;
    public AudioSource audioOfVideo,moleculesSource;
    // Start is called before the first frame update
    void Start()
    {
        // StartCoroutine("PlayProcessOfMoleculesAudio");
    }

    // public IEnumerator PlayProcessOfMoleculesAudio()
    // {
    //     moleculesSource.Play();
    //     yield return null;
    // }

    // public void ShowInstractionMenu()
    // {
    //     StartCoroutine("ShowInstructionDelay");
    // }
    //
    // public IEnumerator ShowInstructionDelay()
    // {
    //
    //     yield return new WaitForSeconds(moleculesSource.clip.length + 7f);
    //     video.SetActive(false);
    //     moleculesProcess.SetActive(false);
    //     instruction.SetActive(true);
    // }

    public void OkButton()
    {
        instruction.SetActive(false);
    }

    public void ReplayAudio(AudioSource source)
    {
        source.Play();
    }
    
    public void ReplayVideo()
    {
        video.GetComponent<VideoPlayer>().Play();
    }
    
    public void CloseVideo()
    {
        video.SetActive(false);
        moleculesProcess.SetActive(false);
        instruction.SetActive(true);
    }

    public void ShowVideo()
    {
        StartCoroutine("ShowVideo_MoleculesProcess");
    }

    public IEnumerator ShowVideo_MoleculesProcess()
    {
        yield return new WaitForSeconds(2f);
        video.SetActive(true);
        moleculesProcess.SetActive(true);
        yield return new WaitForSeconds(20f);
        moleculesSource.Play();
    }
}
