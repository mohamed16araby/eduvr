using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ElementTable : MonoBehaviour
{
    public GameObject[] atoms;
    public AudioSource atomicNoPart1, atomicNoPart2, massNoPart1,massNoPart2, startAudio, finalAudio;
    public GameObject startDataText,finalDataText;
    public Button massNoBtn,atomicNoBtn;
    public void ShowAtom(GameObject atomToShow)
    {
        atomToShow.SetActive(true);
        foreach (var atom in atoms)
        {
            if (atomToShow.name == atom.name)
                continue;
            atom.SetActive(false);
        }

    }

    public void PlayStartAudio()
    {
        finalAudio.Stop();
        atomicNoPart1.Stop();
        atomicNoPart2.Stop();
        StopCoroutine("AtomicNoNarration");
        massNoPart1.Stop();
        massNoPart2.Stop();
        StopCoroutine("MassNoNarration");
        StartCoroutine("StartNarrationAudio");
    }

    public IEnumerator StartNarrationAudio()
    {
        startAudio.Play();
        yield return new WaitForSeconds(startAudio.clip.length);
        massNoBtn.enabled = true;
    }
    public void playAtomicNoNarrationAudio()
    {
        massNoPart1.Stop();
        massNoPart2.Stop();
        atomicNoPart1.Stop();
        atomicNoPart2.Stop();
        startAudio.Stop();
        finalAudio.Stop();
        StopCoroutine("MassNoNarration");
        StopCoroutine("AtomicNoNarration");
        StartCoroutine("AtomicNoNarration");
    }

    public IEnumerator AtomicNoNarration()
    {
        atomicNoPart1.Play();
        yield return new WaitForSeconds(atomicNoPart1.clip.length);
        atomicNoPart2.Play();
        yield return new WaitForSeconds(atomicNoPart2.clip.length);
        finalDataText.SetActive(true);
        finalAudio.Play();
    }
    
    public void playMassNoNarrationAudio()
    {
        atomicNoPart1.Stop();
        atomicNoPart2.Stop();
        massNoPart1.Stop();
        massNoPart2.Stop();
        startAudio.Stop();
        finalAudio.Stop();
        StopCoroutine("AtomicNoNarration");
        StopCoroutine("MassNoNarration");
        StartCoroutine("MassNoNarration");
    }

    public IEnumerator MassNoNarration()
    {
        massNoPart1.Play();
        yield return new WaitForSeconds(massNoPart1.clip.length);
        massNoPart2.Play();
        yield return new WaitForSeconds(massNoPart2.clip.length);
        atomicNoBtn.enabled = true;
    }

    public void PlayFinalAudio()
    {
        startAudio.Stop();
        atomicNoPart1.Stop();
        atomicNoPart2.Stop();
        StopCoroutine("AtomicNoNarration");
        massNoPart1.Stop();
        massNoPart2.Stop();
        StopCoroutine("MassNoNarration");
        finalAudio.Play();
    }
    
    public void ShowDataText(GameObject dataText)
    {
        dataText.SetActive(true);
    }
}
