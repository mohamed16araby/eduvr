using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorObj : MonoBehaviour
{
    private bool rotate;

    // Start is called before the first frame update
    void Start()
    {
        rotate = true; 
    }
    private void OnDisable()
    {
        rotate =false;
    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.activeInHierarchy || rotate)
        {
            transform.Rotate(0,.5f,0);
        }
    }
}
