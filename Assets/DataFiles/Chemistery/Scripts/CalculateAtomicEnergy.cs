using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculateAtomicEnergy : MonoBehaviour
{
    public AtomSO[] atoms;

    private int levelCount;
    private int atomicChange;

    // Start is called before the first frame update
    void Start()
    {
        foreach (var atom in atoms)
        {
            levelCount = (int)atom.period.x;

            atomicChange = atom.atomic;
            
            switch (levelCount)
            {
                case 7:
                    break;
                case 6:
                    break;
                case 5:

                    break;
                case 4:
                    if (atom.atomic - 18 > 2)
                    {
                        atom.atomEnergyLevels[2] += 1;
                        atom.atomEnergyLevels[3] = 2;
                    }
                    else
                        atomicChange = atom.atomic - 2 - 8 - 8;
                        atom.atomEnergyLevels[3] = atomicChange;
                    break;
                case 3:
                    atomicChange = atom.atomic - 2 - 8;
                    atom.atomEnergyLevels[2] = atomicChange;
                    break;
                case 2:
                    atomicChange = atom.atomic - 2;
                    atom.atomEnergyLevels[1] = atomicChange;
                    break;
                case 1:
                    atom.atomEnergyLevels[0] = atomicChange ;
                    break;

                default:
                    break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

[System.Serializable]
public enum period
{
    period_1 = 2,
    period_2 = 8,
    period_3 ,
    period_4 = 2,
    period_5,
    period_6,
    period_7,
}
