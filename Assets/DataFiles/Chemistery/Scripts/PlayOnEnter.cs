using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnEnter : MonoBehaviour
{
    public AudioSource source;
    public AudioClip[] clips;
    public GameObject GameToactive;

    private float delay = 0;
    bool isplayed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (isplayed)
                return;
            if (GameToactive != null)
                GameToactive.SetActive(true);
            Debug.Log("collide");
            for (int i = 0; i < clips.Length; i++)
            {
                StartCoroutine(PlayDelayed(delay, i));
                delay += clips[i].length;
            }
            isplayed = true;
        }
    }

    IEnumerator PlayDelayed(float delay , int index)
    {
        yield return new WaitForSeconds(delay);
        source.clip = clips[index];
        source.Play();
    }
}
