using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LineRendererSmoother : MonoBehaviour
{
    public LineRenderer Line;
    public AnimationCurve lineCurve;
    public List<Vector3> InitialState = new List<Vector3>();
    public float SmoothingLength = 2f;
    public int SmoothingSections = 10;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            SmoothTjeLineCurve();
    }
    public void SmoothTjeLineCurve()
    {
        Debug.Log("it's Working");
        for (float i = 0; i < lineCurve.keys[1].value;  i+=.1f)
        {
            InitialState.Add(new Vector3(0,i,lineCurve.Evaluate(i)));
        }
        Line.GetPositions(InitialState.ToArray());
    }
}
