using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ExamplesManager : MonoBehaviour
{
    [SerializeField] private GameObject mongooseDataText,dolphinDataText,batDataText,penguinDataText,bearDataText
        ,mongooseWave,spiderWave,dolphinWave,fishWave,batWave,butterflyWave
        ,mongoose,dolphin,bat,penguinWalk,penguinIdle,penguinSleep,bear,polarBear,spider,fish,butterfly;
    [SerializeField] private AudioSource mongooseDataSource,dolphinDataSource,batDataSource,penguinDataSource,bearDataSource,painSourceOfAnimals;
    [SerializeField] private VideoPlayer mongooseVideo, dolphinVideo, batVideo,penguinVideo,bearVideo;
    [SerializeField] private Animator mongooseAnimator,dolphinAnimator, batAnimator,penguinAnimator;

    [HideInInspector] private Vector3 originalPosMongoose, originalPosDolphin, originalPosBat,originalPosPenguinWalk,originalPosPenguinIdle,originalPosPenguinSleep,originalPosBear,originalPosPolarBear;
    [HideInInspector] private Quaternion originalRotMongoose, originalRotDolphin, originalRotBat,originalRotPenguinWalk,originalRotPenguinIdle,originalRotPenguinSleep,originalRotBear,originalRotPolarBear;

    private void Start()
    {
        originalPosMongoose = new Vector3(4.231f, 1.309f, 0.585f);
        originalRotMongoose = mongoose.transform.localRotation;
        originalPosDolphin = new Vector3(4.084f, 1.37f, 0.477f);
        originalRotDolphin = dolphin.transform.localRotation;
        originalPosBat = new Vector3(4.172001f, 1.638f, 0.8320003f);
        originalRotBat = bat.transform.localRotation;
        originalPosPenguinWalk = new Vector3(3.741f,1.135f,0.223f);
        originalRotPenguinWalk = penguinWalk.transform.localRotation;
        originalPosPenguinIdle = new Vector3(4.777f,1.135f,1.184f);
        originalRotPenguinIdle = penguinIdle.transform.localRotation;
        originalPosPenguinSleep = new Vector3(4.249f,1.163f,0.505f);
        originalRotPenguinSleep= penguinSleep.transform.localRotation;
        originalPosBear = new Vector3(4.415f,1.118f,-0.643f);
        originalRotBear = bear.transform.localRotation;
        originalPosPolarBear = new Vector3(4.356f, 1.289f, 0.842f);
        originalRotPolarBear = polarBear.transform.localRotation;
    }

    private void Update()
    {
        if (mongooseAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            spider.SetActive(false);
            mongoose.GetComponent<Animator>().enabled = false;
            mongoose.GetComponent<BoxCollider>().enabled = true;
        }
        
        if (dolphinAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            fish.SetActive(false);
            dolphin.GetComponent<Animator>().enabled = false;
            dolphin.GetComponent<BoxCollider>().enabled = true;
        }
        
        if (batAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            butterfly.SetActive(false);
            bat.GetComponent<Animator>().enabled = false;
            bat.GetComponent<BoxCollider>().enabled = true;
        }
        
        if (penguinAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            penguinWalk.GetComponent<Animator>().enabled = false;
            penguinWalk.GetComponent<BoxCollider>().enabled = true;
            penguinIdle.GetComponent<BoxCollider>().enabled = true;
            penguinSleep.GetComponent<BoxCollider>().enabled = true;

        }
    }

    public void PlayMongooseData()
    {
        StartCoroutine("playSoundOfMongooseData");
    }

    public IEnumerator playSoundOfMongooseData()
    {
        yield return new WaitForSeconds(3f);
        mongooseDataText.SetActive(true);
        mongooseDataSource.Play();
        mongooseVideo.loopPointReached += MongooseVideoIsFinished;
    }

    public void MongooseVideoIsFinished(VideoPlayer vp)
    {
        print("MongooseVideoIsFinished");
        mongooseWave.SetActive(false);
        spiderWave.SetActive(false);
        mongooseAnimator.enabled = true;
        StopCoroutine("MongooseWave");
        StopCoroutine("SpiderWave");
    }
    public void PlayDolphinData()
    {
        StartCoroutine("playSoundOfDolphinData");
    }

    public IEnumerator playSoundOfDolphinData()
    {
        yield return new WaitForSeconds(3f);
        dolphinDataText.SetActive(true);
        dolphinDataSource.Play();
        dolphinVideo.loopPointReached += DolphinVideoIsFinished;
    }
    
    public void DolphinVideoIsFinished(VideoPlayer vp)
    {
        print("DolphinVideoIsFinished");
        dolphinWave.SetActive(false);
        fishWave.SetActive(false);
        dolphinAnimator.enabled = true;
        StopCoroutine("DolphinWave");
        StopCoroutine("FishWave");
    }
    
    public void PlayBatData()
    {
        StartCoroutine("playSoundOfBatData");
    }

    public IEnumerator playSoundOfBatData()
    {
        yield return new WaitForSeconds(3f);
        batDataText.SetActive(true);
        batDataSource.Play();
        batVideo.loopPointReached += BatVideoIsFinished;
    }
    
    public void BatVideoIsFinished(VideoPlayer vp)
    {
        print("BatVideoIsFinished");
        batWave.SetActive(false);
        butterflyWave.SetActive(false);
        batAnimator.enabled = true;
        StopCoroutine("BatWave");
        StopCoroutine("ButterflyWave");
    }
    
    public void PlayPenguinData()
    {
        StartCoroutine("playSoundOfPenguinData");
    }

    public IEnumerator playSoundOfPenguinData()
    {
        yield return new WaitForSeconds(3f);
        penguinDataText.SetActive(true);
        penguinDataSource.Play();
        penguinVideo.loopPointReached += PenguinVideoIsFinished;
    }
    
    public void PenguinVideoIsFinished(VideoPlayer vp)
    {
        print("PenguinVideoIsFinished");
        penguinAnimator.enabled = true;
    }
    
    public void PlayBearData()
    {
        StartCoroutine("playSoundOfBearData");
    }

    public IEnumerator playSoundOfBearData()
    {
        yield return new WaitForSeconds(3f);
        bearDataText.SetActive(true);
        bearDataSource.Play();
        bearVideo.loopPointReached += BearVideoIsFinished;
    }
    
    public void BearVideoIsFinished(VideoPlayer vp)
    {
        print("BearVideoIsFinished");
        bear.GetComponent<BoxCollider>().enabled = true;
        polarBear.GetComponent<BoxCollider>().enabled = true;

    }

    #region Mongoose

    public void PlayMongooseWave()
    {
        StartCoroutine("MongooseWave");
    }

    public IEnumerator MongooseWave()
    {
        mongooseWave.SetActive(true);
        mongooseWave.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(mongooseWave.GetComponent<Animation>().clip.length);
        StartCoroutine("SpiderWave");

    }
    
    public IEnumerator SpiderWave()
    {
        spiderWave.SetActive(true);
        spiderWave.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(spiderWave.GetComponent<Animation>().clip.length);
        StartCoroutine("MongooseWave");
    }

    #endregion
    
    #region Dolphin

    public void PlayDolphinWave()
    {
        StartCoroutine("DolphinWave");
    }

    public IEnumerator DolphinWave()
    {
        dolphinWave.SetActive(true);
        dolphinWave.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(dolphinWave.GetComponent<Animation>().clip.length);
        StartCoroutine("FishWave");

    }
    
    public IEnumerator FishWave()
    {
        fishWave.SetActive(true);
        fishWave.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(fishWave.GetComponent<Animation>().clip.length);
        StartCoroutine("DolphinWave");
    }

    #endregion
    
    #region Bat

    public void PlayBatWave()
    {
        StartCoroutine("BatWave");
    }

    public IEnumerator BatWave()
    {
        batWave.SetActive(true);
        batWave.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(batWave.GetComponent<Animation>().clip.length);
        StartCoroutine("ButterflyWave");

    }
    
    public IEnumerator ButterflyWave()
    {
        butterflyWave.SetActive(true);
        butterflyWave.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(butterflyWave.GetComponent<Animation>().clip.length);
        StartCoroutine("BatWave");
    }

    #endregion
    
    public void PlayPainEffect(AudioClip clip)
    {
        painSourceOfAnimals.clip = clip;
        painSourceOfAnimals.Play();
    }
    
    public void OnReleaseObject()
    {
        painSourceOfAnimals.Stop();
        painSourceOfAnimals.clip = null;
    }

    public void ResetMongoosePosition()
    {
        mongoose.transform.localPosition = originalPosMongoose;
        mongoose.transform.localRotation = originalRotMongoose;
    }
    
    public void ResetDolphinPosition()
    {
        dolphin.transform.localPosition = originalPosDolphin;
        dolphin.transform.localRotation = originalRotDolphin;
    }
    
    public void ResetBatPosition()
    {
        bat.transform.localPosition = originalPosBat;
        bat.transform.localRotation = originalRotBat;
    }
    
    public void ResetPenguinWalkPosition()
    {
        penguinWalk.transform.localPosition = originalPosPenguinWalk;
        penguinWalk.transform.localRotation = originalRotPenguinWalk;
    }
    
    public void ResetPenguinIdlePosition()
    {
        penguinIdle.transform.localPosition = originalPosPenguinIdle;
        penguinIdle.transform.localRotation = originalRotPenguinIdle;
    }
    
    public void ResetPenguinSleepPosition()
    {
        penguinSleep.transform.localPosition = originalPosPenguinSleep;
        penguinSleep.transform.localRotation = originalRotPenguinSleep;
    }
    public void ResetBearPosition()
    {
        bear.transform.localPosition = originalPosBear;
        bear.transform.localRotation = originalRotBear;
    }
    
    public void ResetPolarBearPosition()
    {
        polarBear.transform.localPosition = originalPosPolarBear;
        polarBear.transform.localRotation = originalRotPolarBear;
    }
}
