using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SencesManager : MonoBehaviour
{
    [SerializeField] private GameObject[] Signals;
    [SerializeField] private GameObject brainHighlite;
    [SerializeField] private AudioSource source;

    public void ShowDataSence(GameObject Data)
    {
        Data.SetActive(true);
    }

    public void ShowSignals(GameObject signal)
    {
        signal.SetActive(true);
        brainHighlite.SetActive(false);
        for (int i = 0; i < Signals.Length; i++)
        {
            Signals[i].SetActive(false);
            signal.SetActive(true);
            brainHighlite.SetActive(true);
        }
    }

    public void PlayNarration(AudioClip clip)
    {
        source.clip = clip;
        source.Play();
    }
    
}