using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;
using UnityEngine.SceneManagement;

public class LoadSceneManager : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (InputBridge.Instance.AButtonDown)
        {
            SceneManager.LoadScene("DigestiveSystem");
        }
            
    }
}
