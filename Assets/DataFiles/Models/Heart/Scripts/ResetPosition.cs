using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class ResetPosition : MonoBehaviour
{
    public GameObject Aorta, RightVentricle, RightAtrium, LeftAtrium;
    private Vector3 initialAorta, initialRightVentricle, initialRightAtrium, initialLeftAtrium;

    private void Start()
    {
        initialAorta = Aorta.transform.position;
        initialRightVentricle = RightVentricle.transform.position;
        initialRightAtrium = RightAtrium.transform.position;
        initialLeftAtrium = LeftAtrium.transform.position;
    }

    public void Update()
    {
        if (InputBridge.Instance.XButtonDown) {
            ResetHeartPosition();
        }
    }

    public void ResetHeartPosition() {
        Aorta.transform.position = initialAorta;
        RightVentricle.transform.position = initialRightVentricle;
        RightAtrium.transform.position = initialRightAtrium;
        LeftAtrium.transform.position = initialLeftAtrium;

    }
}
