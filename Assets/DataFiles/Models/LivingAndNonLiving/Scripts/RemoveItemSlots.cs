using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class RemoveItemSlots : MonoBehaviour
{
    public GameObject LivingSlot, NonLivingSlot;

    public void RemoveItemSlotLiving() {
        LivingSlot.GetComponent<SnapZone>().HeldItem.GetComponent<Rigidbody>().useGravity = true;
        LivingSlot.GetComponent<SnapZone>().HeldItem.GetComponent<Rigidbody>().isKinematic = false;
        LivingSlot.GetComponent<SnapZone>().HeldItem.GetComponent<BoxCollider>().enabled = true;
        LivingSlot.GetComponent<SnapZone>().HeldItem = null;
    }

    public void RemoveItemSlotNoLiving() {

        NonLivingSlot.GetComponent<SnapZone>().HeldItem.GetComponent<Rigidbody>().useGravity = true;
        NonLivingSlot.GetComponent<SnapZone>().HeldItem.GetComponent<Rigidbody>().isKinematic = false;
        NonLivingSlot.GetComponent<SnapZone>().HeldItem.GetComponent<BoxCollider>().enabled = true;
        NonLivingSlot.GetComponent<SnapZone>().HeldItem = null;
    }
}
