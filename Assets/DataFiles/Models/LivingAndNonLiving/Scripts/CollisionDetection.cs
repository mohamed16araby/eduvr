using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    public AudioSource wrongSource;
    public bool isLiving, isNonLiving;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Living" && isLiving == false) {
            print("collision With Living");
            wrongSource.Play();
        }

        if (other.gameObject.tag == "NonLiving" && isNonLiving == false)
        {
            print("collision With NOnLiving");
            wrongSource.Play();
        }
    }
}
