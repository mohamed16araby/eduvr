using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTour : MonoBehaviour
{
    [SerializeField] private float speed;
    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(0f,1*speed*Time.deltaTime ,0f);
    }
}
