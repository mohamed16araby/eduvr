using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class ResetPartsAnimatedMULti : MonoBehaviour
{
    /////Parts x
    public GameObject p1, p2, p3, p4,p5,p6,p7;
    private Vector3 ppart1, ppart2, ppart3, ppart4, ppart5,ppart6,ppart7;
    private Quaternion Rpart1, Rpart2, Rpart3, Rpart4, Rpart5,Rpart6, Rpart7;
    
    
    private void Awake()
    {
        ppart1 = p1.transform.position;
        ppart2 = p2.transform.position;
        ppart3 = p3.transform.position;
        ppart4 = p4.transform.position;
        ppart5 = p5.transform.position;
        ppart6 = p6.transform.position;
        ppart7 = p7.transform.position;

        Rpart1 = p1.transform.rotation;
        Rpart2 = p2.transform.rotation;
        Rpart3 = p3.transform.rotation;
        Rpart4 = p4.transform.rotation;
        Rpart5 = p5.transform.rotation;
        Rpart6 = p4.transform.rotation;
        Rpart7 = p4.transform.rotation;

    }

    private void Update()
    {
        if (InputBridge.Instance.XButtonDown)
        {
            Resettheitems();
        }
    }

    public void Resettheitems()
    {

        LeanTween.move(p1, ppart1, 2f).setEase(LeanTweenType.linear);
        LeanTween.move(p2, ppart2, 2f).setEase(LeanTweenType.linear);
        LeanTween.move(p3, ppart3, 2f).setEase(LeanTweenType.linear);
        LeanTween.move(p4, ppart4, 2f).setEase(LeanTweenType.linear);
        LeanTween.move(p5, ppart5, 2f).setEase(LeanTweenType.linear);
        LeanTween.move(p6, ppart6, 2f).setEase(LeanTweenType.linear);
        LeanTween.move(p7, ppart7, 2f).setEase(LeanTweenType.linear);

        p1.transform.rotation = Rpart1;
        p2.transform.rotation = Rpart2;
        p3.transform.rotation = Rpart3;
        p4.transform.rotation = Rpart4;
        p5.transform.rotation = Rpart5;
        p6.transform.rotation = Rpart6;
        p7.transform.rotation = Rpart7;

    }
}
