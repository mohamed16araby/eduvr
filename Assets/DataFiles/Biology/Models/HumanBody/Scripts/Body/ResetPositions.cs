using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResetPositions : MonoBehaviour
{
    public TMP_Text objectname;
    private Vector3 intialposhead, intialposheart, intialposrighthand, intialposlefthand, intialposleg;
    public GameObject head, heart, righthand, lefthand, leg;


    // Start is called before the first frame update
    void Start()
    {
        intialposhead = head.transform.position;
        intialposheart = heart.transform.position;
        intialposrighthand = righthand.transform.position;
        intialposlefthand = lefthand.transform.position;
        intialposleg = leg.transform.position; 


    }

    public void ResetSpecificPotion() {
        head.transform.position = intialposhead;
        heart.transform.position = intialposheart;
        righthand.transform.position = intialposrighthand;
        lefthand.transform.position = intialposlefthand;
        leg.transform.position = intialposleg;

    }

    public void SetName(string name) {
        objectname.text = name; 
    }

    public void resetrotation() {

    }



}
