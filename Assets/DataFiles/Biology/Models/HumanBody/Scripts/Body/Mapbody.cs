using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mapbody : MonoBehaviour
{
    public GameObject humanbody, mapbody;
    public void EnableMapbody() {
        humanbody.SetActive(false);
        mapbody.SetActive(true);
    }
}
