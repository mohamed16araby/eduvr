using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyManager : MonoBehaviour
{
    public GameObject viens, stomach, humanbody,maphuman;
    public GameObject [] body;
    private Vector3 initialvienspos, initialstomachpos;
    private void Start()
    {
        initialvienspos = viens.transform.position;
        initialstomachpos = stomach.transform.position;
    }
    public void Hidebody() {
        for (int i = 0; i < body.Length; i++) {
            body[i].SetActive(false);
            humanbody.SetActive(true);
        }
        maphuman.SetActive(false);
    }

    public void ResetPostion() {
        viens.transform.position = initialvienspos;
        viens.transform.eulerAngles =new Vector3(0f, -180f, 0f);
        stomach.transform.eulerAngles = new Vector3(0f, -180f, 0f);
        stomach.transform.position = initialstomachpos;
    }


    public void ShowMapHUman() {
        humanbody.SetActive(false);
        maphuman.SetActive(true);
       // Hidebody();


    }

}
