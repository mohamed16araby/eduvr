using System;
using System.Collections;
using System.Collections.Generic;
using BNG;
using UnityEngine;

public class ResetpR : MonoBehaviour
{
    public GameObject brain, nerves;
    public bool flag;
    private Vector3 mPosition;
    public GameObject p1, p2, p3, p4;
    private Vector3 ppart1, ppart2, ppart3, ppart4;
    private Quaternion Rpart1, Rpart2, Rpart3, Rpart4;
    private Vector3 Spart1, Spart2, Spart3;
    private float acceleration = 0.05f;
    private float movementSpeed = 0f;
    private float rotationSpeed = 180f;
    private bool isJumping = false;
    private Vector2 touchAxis;
    private float triggerAxis;
    private Rigidbody rb;
    private Vector3 defaultPosition;
    private Vector3 defaulScale;
    static float step;
    private Quaternion defaultRotation;
    public float speed = 1.0f;

    void Start()
    {
        step = speed * Time.deltaTime;

        ppart1 = p1.transform.position;
        ppart2 = p2.transform.position;
        ppart3 = p3.transform.position;
        ppart4 = p4.transform.position;

        Rpart1 = p1.transform.rotation;
        Rpart2 = p2.transform.rotation;
        Rpart3 = p3.transform.rotation;
        Rpart4 = p4.transform.rotation;
    }

    private void Update()
    {
        if (InputBridge.Instance.XButtonDown)
        {
            Resettheitems();
        }
    }

    public void Resettheitems()
    {
        if (brain.activeSelf)
        {
            LeanTween.move(p1, ppart1, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p2, ppart2, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p3, ppart3, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p4, ppart4, 2f).setEase(LeanTweenType.linear);
            p1.transform.rotation = Rpart1;
            p2.transform.rotation = Rpart2;
            p3.transform.rotation = Rpart3;
            p4.transform.rotation = Rpart4;
        }
        
        if (nerves.activeSelf)
        {
            LeanTween.move(p1, ppart1, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p2, ppart2, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p3, ppart3, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p4, ppart4, 2f).setEase(LeanTweenType.linear);
            p1.transform.rotation = Rpart1;
            p2.transform.rotation = Rpart2;
            p3.transform.rotation = Rpart3;
            p4.transform.rotation = Rpart4;
        }
        else
        {
            print("Object To Reset Is Not Active");
        }
    }
}