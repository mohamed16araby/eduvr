﻿namespace VRTK.Examples
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
using BNG;

    public class ButtonsoptionMuscelse : MonoBehaviour
    {

        public float maxAcceleration = 3f;
        public float jumpPower = 10f;

        private float acceleration = 0.05f;
        private float movementSpeed = 0f;
        private float rotationSpeed = 180f;
        private bool isJumping = false;
        private Vector2 touchAxis;
        private float triggerAxis;
        private Rigidbody rb;
        private Vector3 defaultPosition;
        private Vector3 defaulScale;
        static float step;
        private Quaternion defaultRotation;
        public float speed = 1.0f;


        /////Parts 
        public GameObject p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36
            ,p37, p38, p39, p40, p41, p42, p43, p44, p45, p46, p47, p48, p49, p50, p51, p52, p53, p54, p55, p56, p57, p58, p59, p60, p61, p62, p63, p64, p65, p66, p67, p68, p69, p70, p71, p72
            ,p73, p74, p75, p76, p77, p78, p79, p80, p81, p82, p83, p84, p85, p86, p87, p88, p89, p90, p91, p92, p93,p94, p95, p96, p97, p98, p99, p100, p101, p102, p103, p104, p105, p106, p107, p108
            ,p109, p110, p111, p112, p113, p114, p115, p116, p117, p118, p119, p120, p121, p122, p123, p124, p125, p126, p127, p128, p129, p130, p131, p132, p133, p134, p135, p136, p137, p138, p139, p140, p141, p142, p143;
        private Vector3 ppart1, ppart2, ppart3, ppart4, ppart5, ppart6, ppart7, ppart8, ppart9, ppart10, ppart11, ppart12, ppart13, ppart14, ppart15, ppart16, ppart17, ppart18
        ,ppart19, ppart20, ppart21, ppart22, ppart23, ppart24, ppart25, ppart26, ppart27, ppart28, ppart29, ppart30, ppart31, ppart32, ppart33, ppart34, ppart35, ppart36
        ,ppart37, ppart38, ppart39, ppart40, ppart41, ppart42, ppart43, ppart44, ppart45, ppart46, ppart47, ppart48, ppart49, ppart50, ppart51, ppart52, ppart53, ppart54
        ,ppart55, ppart56, ppart57, ppart58, ppart59, ppart60, ppart61, ppart62, ppart63, ppart64, ppart65, ppart66, ppart67, ppart68, ppart69, ppart70, ppart71, ppart72
        ,ppart73, ppart74, ppart75, ppart76, ppart77, ppart78, ppart79, ppart80, ppart81, ppart82, ppart83, ppart84, ppart85, ppart86, ppart87, ppart88, ppart89, ppart90
        ,ppart91, ppart92, ppart93, ppart94, ppart95, ppart96, ppart97, ppart98, ppart99, ppart100, ppart101, ppart102, ppart103, ppart104, ppart105, ppart106, ppart107, ppart108
        ,ppart109, ppart110, ppart111, ppart112, ppart113, ppart114, ppart115, ppart116, ppart117, ppart118, ppart119, ppart120, ppart121, ppart122, ppart123, ppart124, ppart125, ppart126
        ,ppart127, ppart128, ppart129, ppart130, ppart131, ppart132, ppart133, ppart134, ppart135, ppart136, ppart137, ppart138, ppart139, ppart140, ppart141, ppart142, ppart143;

        //  private Quaternion Rpart1, Rpart2, Rpart3, Rpart4, Rpart5, Rpart6, Rpart7, Rpart8, Rpart9, Rpart10, Rpart11;
        //    private Vector3 Spart1, Spart2, Spart3, Spart4, Spart5, Spart6, Spart7, Spart8, Spart9, Spart10, Spart11;
        //private Vector3 Spart1, Spart2, Spart3, Spart4, Spart5, Spart6, Spart7, Spart8, Spart9, Spart10, Spart11, Spart12, Spart13, Spart14, Spart15, Spart16, Spart17, Spart18,
     // Spart19, Spart20, Spart21, Spart22, Spart23, Spart24, Spart25, Spart26, Spart27, Spart28, Spart29, Spart30, Spart31, Spart32, Spart33, Spart34, Spart35, Spart36;

        private Quaternion Rpart1, Rpart2, Rpart3, Rpart4, Rpart5, Rpart6, Rpart7, Rpart8, Rpart9, Rpart10, Rpart11, Rpart12, Rpart13, Rpart14, Rpart15, Rpart16, Rpart17, Rpart18,
      Rpart19, Rpart20, Rpart21, Rpart22, Rpart23, Rpart24, Rpart25, Rpart26, Rpart27, Rpart28, Rpart29, Rpart30, Rpart31, Rpart32, Rpart33, Rpart34, Rpart35, Rpart36
      ,Rpart37, Rpart38, Rpart39, Rpart40, Rpart41, Rpart42, Rpart43, Rpart44, Rpart45, Rpart46, Rpart47, Rpart48, Rpart49, Rpart50, Rpart51, Rpart52, Rpart53, Rpart54
      ,Rpart55, Rpart56, Rpart57, Rpart58, Rpart59, Rpart60, Rpart61, Rpart62, Rpart63, Rpart64, Rpart65, Rpart66, Rpart67, Rpart68, Rpart69, Rpart70, Rpart71, Rpart72
      ,Rpart73, Rpart74, Rpart75, Rpart76, Rpart77, Rpart78, Rpart79, Rpart80, Rpart81, Rpart82, Rpart83, Rpart84, Rpart85, Rpart86, Rpart87, Rpart88, Rpart89, Rpart90
      ,Rpart91, Rpart92, Rpart93, Rpart94, Rpart95, Rpart96, Rpart97, Rpart98, Rpart99, Rpart100, Rpart101, Rpart102, Rpart103, Rpart104, Rpart105, Rpart106, Rpart107, Rpart108
      ,Rpart109, Rpart110, Rpart111, Rpart112, Rpart113, Rpart114, Rpart115, Rpart116, Rpart117, Rpart118, Rpart119, Rpart120, Rpart121, Rpart122, Rpart123, Rpart124, Rpart125, Rpart126
      ,Rpart127, Rpart128, Rpart129, Rpart130, Rpart131, Rpart132, Rpart133, Rpart134, Rpart135, Rpart136, Rpart137, Rpart138, Rpart139, Rpart140, Rpart141, Rpart142, Rpart143;




        private void Awake()
        {

            // step = 10f;
            step = speed * Time.deltaTime; // calculate distance to move
                                           ///////PARTS POSATION
            ppart1 = p1.transform.position;
            ppart2 = p2.transform.position;
            ppart3 = p3.transform.position;
            ppart4 = p4.transform.position;
            ppart5 = p5.transform.position;
            ppart6 = p6.transform.position;
            ppart7 = p7.transform.position;
            ppart8 = p8.transform.position;
            ppart9 = p9.transform.position;
            ppart10 = p10.transform.position;
            ppart11 = p11.transform.position;
            ppart12 = p12.transform.position;
            ppart13 = p13.transform.position;
            ppart14 = p14.transform.position;
            ppart15 = p15.transform.position;
            ppart16 = p16.transform.position;
            ppart17 = p17.transform.position;
            ppart18 = p18.transform.position;
            ppart19 = p19.transform.position;
            ppart20 = p20.transform.position;
            ppart21 = p21.transform.position;
            ppart22 = p22.transform.position;
            ppart23 = p23.transform.position;
            ppart24 = p24.transform.position;
            ppart25 = p25.transform.position;
            ppart26 = p26.transform.position;
            ppart27 = p27.transform.position;
            ppart28 = p28.transform.position;
            ppart29 = p29.transform.position;
            ppart30 = p30.transform.position;
            ppart31 = p31.transform.position;
            ppart32 = p32.transform.position;
            ppart33 = p33.transform.position;
            ppart34 = p34.transform.position;
            ppart35 = p35.transform.position;
            ppart36 = p36.transform.position;
            ppart37 = p37.transform.position;
            ppart38 = p38.transform.position;
            ppart39 = p39.transform.position;
            ppart40 = p40.transform.position;
            ppart41 = p41.transform.position;
            ppart42 = p42.transform.position;
            ppart43 = p43.transform.position;
            ppart44 = p44.transform.position;
            ppart45 = p45.transform.position;
            ppart46 = p46.transform.position;
            ppart47 = p47.transform.position;
            ppart48 = p48.transform.position;
            ppart49 = p49.transform.position;
            ppart50 = p50.transform.position;
            ppart51 = p51.transform.position;
            ppart52 = p52.transform.position;
            ppart53 = p53.transform.position;
            ppart54 = p54.transform.position;
            ppart55 = p55.transform.position;
            ppart56 = p56.transform.position;
            ppart57 = p57.transform.position;
            ppart58 = p58.transform.position;
            ppart59 = p59.transform.position;
            ppart60 = p60.transform.position;
            ppart61 = p61.transform.position;
            ppart62 = p62.transform.position;
            ppart63 = p63.transform.position;
            ppart64 = p64.transform.position;
            ppart65 = p65.transform.position;
            ppart66 = p66.transform.position;
            ppart67 = p67.transform.position;
            ppart68 = p68.transform.position;
            ppart69 = p69.transform.position;
            ppart70 = p70.transform.position;
            ppart71 = p71.transform.position;
            ppart72 = p72.transform.position;
            ppart73 = p73.transform.position;
            ppart74 = p74.transform.position;
            ppart75 = p75.transform.position;
            ppart76 = p76.transform.position;
            ppart77 = p77.transform.position;
            ppart78 = p78.transform.position;
            ppart79 = p79.transform.position;
            ppart80 = p80.transform.position;
            ppart81 = p81.transform.position;
            ppart82 = p82.transform.position;
            ppart83 = p83.transform.position;
            ppart84 = p84.transform.position;
            ppart85 = p85.transform.position;
            ppart86 = p86.transform.position;
            ppart87 = p87.transform.position;
            ppart88 = p88.transform.position;
            ppart89 = p89.transform.position;
            ppart90 = p90.transform.position;
            ppart91 = p91.transform.position;
            ppart92 = p92.transform.position;
            ppart93 = p93.transform.position;
            ppart94 = p94.transform.position;
            ppart95 = p95.transform.position;
            ppart96 = p96.transform.position;
            ppart97 = p97.transform.position;
            ppart98 = p98.transform.position;
            ppart99 = p99.transform.position;
            ppart100= p100.transform.position;
            ppart101= p101.transform.position;
            ppart102= p102.transform.position;
            ppart103= p103.transform.position;
            ppart104= p104.transform.position;
            ppart105= p105.transform.position;
            ppart106= p106.transform.position;
            ppart107= p107.transform.position;
            ppart108= p108.transform.position;
            ppart109= p109.transform.position;
            ppart110= p110.transform.position;
            ppart111= p111.transform.position;
            ppart112= p112.transform.position;
            ppart113= p113.transform.position;
            ppart114= p114.transform.position;
            ppart115= p115.transform.position;
            ppart116= p116.transform.position;
            ppart117= p117.transform.position;
            ppart118= p118.transform.position;
            ppart119= p119.transform.position;
            ppart120= p120.transform.position;
            ppart121= p121.transform.position;
            ppart122= p122.transform.position;
            ppart123= p123.transform.position;
            ppart124= p124.transform.position;
            ppart125= p125.transform.position;
            ppart126= p126.transform.position;
            ppart127= p127.transform.position;
            ppart128= p128.transform.position;
            ppart129= p129.transform.position;
            ppart130= p130.transform.position;
            ppart131= p131.transform.position;
            ppart132= p132.transform.position;
            ppart133= p133.transform.position;
            ppart134= p134.transform.position;
            ppart135= p135.transform.position;
            ppart136= p136.transform.position;
            ppart137= p137.transform.position;
            ppart138= p138.transform.position;
            ppart139= p139.transform.position;
            ppart140= p140.transform.position;
            ppart141= p141.transform.position;
            ppart142= p142.transform.position;
            ppart143= p143.transform.position;

            
            

            Rpart1 = p1.transform.rotation;
            Rpart2 = p2.transform.rotation;
            Rpart3 = p3.transform.rotation;
            Rpart4 = p4.transform.rotation;
            Rpart5 = p5.transform.rotation;
            Rpart6 = p6.transform.rotation;
            Rpart7 = p7.transform.rotation;
            Rpart8 = p8.transform.rotation;
            Rpart9 = p9.transform.rotation;
            Rpart10 = p10.transform.rotation;
            Rpart11 = p11.transform.rotation;
            Rpart12 = p12.transform.rotation;
            Rpart13 = p13.transform.rotation;
            Rpart14 = p14.transform.rotation;
            Rpart15 = p15.transform.rotation;
            Rpart16 = p16.transform.rotation;
            Rpart17 = p17.transform.rotation;
            Rpart18 = p18.transform.rotation;
            Rpart19 = p19.transform.rotation;
            Rpart20 = p20.transform.rotation;
            Rpart21 = p21.transform.rotation;
            Rpart22 = p22.transform.rotation;
            Rpart23 = p23.transform.rotation;
            Rpart24 = p24.transform.rotation;
            Rpart25 = p25.transform.rotation;
            Rpart26 = p26.transform.rotation;
            Rpart27 = p27.transform.rotation;
            Rpart28 = p28.transform.rotation;
            Rpart29 = p29.transform.rotation;
            Rpart30 = p30.transform.rotation;
            Rpart31 = p31.transform.rotation;
            Rpart32 = p32.transform.rotation;
            Rpart33 = p33.transform.rotation;
            Rpart34 = p34.transform.rotation;
            Rpart35 = p35.transform.rotation;
            Rpart36 = p36.transform.rotation;
            Rpart37 = p37.transform.rotation;
            Rpart38 = p38.transform.rotation;
            Rpart39 = p39.transform.rotation;
            Rpart40 = p40.transform.rotation;
            Rpart41 = p41.transform.rotation;
            Rpart42 = p42.transform.rotation;
            Rpart43 = p43.transform.rotation;
            Rpart44 = p44.transform.rotation;
            Rpart45 = p45.transform.rotation;
            Rpart46 = p46.transform.rotation;
            Rpart47 = p47.transform.rotation;
            Rpart48 = p48.transform.rotation;
            Rpart49 = p49.transform.rotation;
            Rpart50 = p50.transform.rotation;
            Rpart51 = p51.transform.rotation;
            Rpart52 = p52.transform.rotation;
            Rpart53 = p53.transform.rotation;
            Rpart54 = p54.transform.rotation;
            Rpart55 = p55.transform.rotation;
            Rpart56 = p56.transform.rotation;
            Rpart57 = p57.transform.rotation;
            Rpart58 = p58.transform.rotation;
            Rpart59 = p59.transform.rotation;
            Rpart60 = p60.transform.rotation;
            Rpart61 = p61.transform.rotation;
            Rpart62 = p62.transform.rotation;
            Rpart63 = p63.transform.rotation;
            Rpart64 = p64.transform.rotation;
            Rpart65 = p65.transform.rotation;
            Rpart66 = p66.transform.rotation;
            Rpart67 = p67.transform.rotation;
            Rpart68 = p68.transform.rotation;
            Rpart69 = p69.transform.rotation;
            Rpart70 = p70.transform.rotation;
            Rpart71 = p71.transform.rotation;
            Rpart72 = p72.transform.rotation;
            Rpart73 = p73.transform.rotation;
            Rpart74 = p74.transform.rotation;
            Rpart75 = p75.transform.rotation;
            Rpart76 = p76.transform.rotation;
            Rpart77 = p77.transform.rotation;
            Rpart78 = p78.transform.rotation;
            Rpart79 = p79.transform.rotation;
            Rpart80 = p80.transform.rotation;
            Rpart81 = p81.transform.rotation;
            Rpart82 = p82.transform.rotation;
            Rpart83 = p83.transform.rotation;
            Rpart84 = p84.transform.rotation;
            Rpart85 = p85.transform.rotation;
            Rpart86 = p86.transform.rotation;
            Rpart87 = p87.transform.rotation;
            Rpart88 = p88.transform.rotation; 
            Rpart89 = p89.transform.rotation;
            Rpart90 = p90.transform.rotation;
            Rpart91 = p91.transform.rotation;
            Rpart92 = p92.transform.rotation;
            Rpart93 = p93.transform.rotation;
            Rpart94 = p94.transform.rotation;
            Rpart95 = p95.transform.rotation;
            Rpart96 = p96.transform.rotation;
            Rpart97 = p97.transform.rotation;
            Rpart98 = p98.transform.rotation;
            Rpart99 = p99.transform.rotation;
            Rpart100= p100.transform.rotation;
            Rpart101= p101.transform.rotation;
            Rpart102= p102.transform.rotation;
            Rpart103= p103.transform.rotation;
            Rpart104= p104.transform.rotation;
            Rpart105= p105.transform.rotation;
            Rpart106= p106.transform.rotation;
            Rpart107= p107.transform.rotation;
            Rpart108= p108.transform.rotation;
            Rpart109= p109.transform.rotation;
            Rpart110= p110.transform.rotation;
            Rpart111= p111.transform.rotation;
            Rpart112= p112.transform.rotation;
            Rpart113= p113.transform.rotation;
            Rpart114= p114.transform.rotation;
            Rpart115= p115.transform.rotation;
            Rpart116= p116.transform.rotation;
            Rpart117= p117.transform.rotation;
            Rpart118= p118.transform.rotation;
            Rpart119= p119.transform.rotation;
            Rpart120= p120.transform.rotation;
            Rpart121= p121.transform.rotation;
            Rpart122= p122.transform.rotation;
            Rpart123= p123.transform.rotation;
            Rpart124= p124.transform.rotation;
            Rpart125= p125.transform.rotation;
            Rpart126= p126.transform.rotation;
            Rpart127= p127.transform.rotation;
            Rpart128= p128.transform.rotation;
            Rpart129= p129.transform.rotation;
            Rpart130= p130.transform.rotation;
            Rpart131= p131.transform.rotation;
            Rpart132= p132.transform.rotation;
            Rpart133= p133.transform.rotation;
            Rpart134= p134.transform.rotation;
            Rpart135= p135.transform.rotation;
            Rpart136= p136.transform.rotation;
            Rpart137= p137.transform.rotation;
            Rpart138= p138.transform.rotation;
            Rpart139= p139.transform.rotation;
            Rpart140= p140.transform.rotation;
            Rpart141= p141.transform.rotation;
            Rpart142= p142.transform.rotation;
            Rpart143= p143.transform.rotation;
            
            
        }
         
         private void Update()
                     {
                         if (InputBridge.Instance.XButtonDown)
                         {
                             Resettheitems();
                         }
                     }
         
         
         public void Resettheitems()
        {

            LeanTween.move(p1, ppart1, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p2, ppart2, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p3, ppart3, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p4, ppart4, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p5, ppart5, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p6, ppart6, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p7, ppart7, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p8, ppart8, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p9, ppart9, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p10, ppart10, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p11, ppart11, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p12, ppart12, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p13, ppart13, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p14, ppart14, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p15, ppart15, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p16, ppart16, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p17, ppart17, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p18, ppart18, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p19, ppart19, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p20, ppart20, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p21, ppart21, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p22, ppart22, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p23, ppart23, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p24, ppart24, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p25, ppart25, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p26, ppart26, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p27, ppart27, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p28, ppart28, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p29, ppart29, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p30, ppart30, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p31, ppart31, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p32, ppart32, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p33, ppart33, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p34, ppart34, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p35, ppart35, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p36, ppart36, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p37, ppart37, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p38, ppart38, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p39, ppart39, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p40, ppart40, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p41, ppart41, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p42, ppart42, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p43, ppart43, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p44, ppart44, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p45, ppart45, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p46, ppart46, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p47, ppart47, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p48, ppart48, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p49, ppart49, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p50, ppart50, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p51, ppart51, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p52, ppart52, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p53, ppart53, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p54, ppart54, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p55, ppart55, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p56, ppart56, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p57, ppart57, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p58, ppart58, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p59, ppart59, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p60, ppart60, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p61, ppart61, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p62, ppart62, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p63, ppart63, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p64, ppart64, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p65, ppart65, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p66, ppart66, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p67, ppart67, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p68, ppart68, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p69, ppart69, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p70, ppart70, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p71, ppart71, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p72, ppart72, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p73, ppart73, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p74, ppart74, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p75, ppart75, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p76, ppart76, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p77, ppart77, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p78, ppart78, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p79, ppart79, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p80, ppart80, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p81, ppart81, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p82, ppart82, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p83, ppart83, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p84, ppart84, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p85, ppart85, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p86, ppart86, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p87, ppart87, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p88, ppart88, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p89, ppart89, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p90, ppart90, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p91, ppart91, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p92, ppart92, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p93, ppart93, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p94, ppart94, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p95, ppart95, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p96, ppart96, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p97, ppart97, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p98, ppart98, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p99, ppart99, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p100, ppart100, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p101, ppart101, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p102, ppart102, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p103, ppart103, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p104, ppart104, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p105, ppart105, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p106, ppart106, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p107, ppart107, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p108, ppart108, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p109, ppart109, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p110, ppart110, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p111, ppart111, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p112, ppart112, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p113, ppart113, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p114, ppart114, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p115, ppart115, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p116, ppart116, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p117, ppart117, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p118, ppart118, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p119, ppart119, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p120, ppart120, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p121, ppart121, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p122, ppart122, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p123, ppart123, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p124, ppart124, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p125, ppart125, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p126, ppart126, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p127, ppart127, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p128, ppart128, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p129, ppart129, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p130, ppart130, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p131, ppart131, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p132, ppart132, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p133, ppart133, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p134, ppart134, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p135, ppart135, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p136, ppart136, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p137, ppart137, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p138, ppart138, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p139, ppart139, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p140, ppart140, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p141, ppart141, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p142, ppart142, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p143, ppart143, 2f).setEase(LeanTweenType.linear);




            p1.transform.rotation = Rpart1;
            p2.transform.rotation = Rpart2;
            p3.transform.rotation = Rpart3;
            p4.transform.rotation = Rpart4;
            p5.transform.rotation = Rpart5;
            p6.transform.rotation = Rpart6;
            p7.transform.rotation = Rpart7;
            p8.transform.rotation = Rpart8;
            p9.transform.rotation = Rpart9;
            p10.transform.rotation = Rpart10;
            p11.transform.rotation = Rpart11;
            p12.transform.rotation = Rpart12;
            p13.transform.rotation = Rpart13;
            p14.transform.rotation = Rpart14;
            p15.transform.rotation = Rpart15;
            p16.transform.rotation = Rpart16;
            p17.transform.rotation = Rpart17;
            p18.transform.rotation = Rpart18;
            p19.transform.rotation = Rpart19;
            p20.transform.rotation = Rpart20;
            p21.transform.rotation = Rpart21;
            p22.transform.rotation = Rpart22;
            p23.transform.rotation = Rpart23;
            p24.transform.rotation = Rpart24;
            p25.transform.rotation = Rpart25;
            p26.transform.rotation = Rpart26;
            p27.transform.rotation = Rpart27;
            p28.transform.rotation = Rpart28;
            p29.transform.rotation = Rpart29;
            p30.transform.rotation = Rpart30;
            p31.transform.rotation = Rpart31;
            p32.transform.rotation = Rpart32;
            p33.transform.rotation = Rpart33;
            p34.transform.rotation = Rpart34;
            p35.transform.rotation = Rpart35;
            p36.transform.rotation = Rpart36;
            p37.transform.rotation = Rpart37;
            p38.transform.rotation = Rpart38;
            p39.transform.rotation = Rpart39;
            p40.transform.rotation = Rpart40;
            p41.transform.rotation = Rpart41;
            p42.transform.rotation = Rpart42;
            p43.transform.rotation = Rpart43;
            p44.transform.rotation = Rpart44;
            p45.transform.rotation = Rpart45;
            p46.transform.rotation = Rpart46;
            p47.transform.rotation = Rpart47;
            p48.transform.rotation = Rpart48;
            p49.transform.rotation = Rpart49;
            p50.transform.rotation = Rpart50;
            p51.transform.rotation = Rpart51;
            p52.transform.rotation = Rpart52;
            p53.transform.rotation = Rpart53;
            p54.transform.rotation = Rpart54;
            p55.transform.rotation = Rpart55;
            p56.transform.rotation = Rpart56;
            p57.transform.rotation = Rpart57;
            p58.transform.rotation = Rpart58;
            p59.transform.rotation = Rpart59;
            p60.transform.rotation = Rpart60;
            p61.transform.rotation = Rpart61;
            p62.transform.rotation = Rpart62;
            p63.transform.rotation = Rpart63;
            p64.transform.rotation = Rpart64;
            p65.transform.rotation = Rpart65;
            p66.transform.rotation = Rpart66;
            p67.transform.rotation = Rpart67;
            p68.transform.rotation = Rpart68;
            p69.transform.rotation = Rpart69;
            p70.transform.rotation = Rpart70;
            p71.transform.rotation = Rpart71;
            p72.transform.rotation = Rpart72;
            p73.transform.rotation = Rpart73;
            p74.transform.rotation = Rpart74;
            p75.transform.rotation = Rpart75;
            p76.transform.rotation = Rpart76;
            p77.transform.rotation = Rpart77;
            p78.transform.rotation = Rpart78;
            p79.transform.rotation = Rpart79;
            p80.transform.rotation = Rpart80;
            p81.transform.rotation = Rpart81;
            p82.transform.rotation = Rpart82;
            p83.transform.rotation = Rpart83;
            p84.transform.rotation = Rpart84;
            p85.transform.rotation = Rpart85;
            p86.transform.rotation = Rpart86;
            p87.transform.rotation = Rpart87;
            p88.transform.rotation = Rpart88;
            p89.transform.rotation = Rpart89;
            p90.transform.rotation = Rpart90;
            p91.transform.rotation = Rpart91;
            p92.transform.rotation = Rpart92;
            p93.transform.rotation = Rpart93;
            p94.transform.rotation = Rpart94;
            p95.transform.rotation = Rpart95;
            p96.transform.rotation = Rpart96;
            p97.transform.rotation = Rpart97;
            p98.transform.rotation = Rpart98;
            p99.transform.rotation = Rpart99;
            p100.transform.rotation = Rpart100;
            p101.transform.rotation = Rpart101;
            p102.transform.rotation = Rpart102;
            p103.transform.rotation = Rpart103;
            p104.transform.rotation = Rpart104;
            p105.transform.rotation = Rpart105;
            p106.transform.rotation = Rpart106;
            p107.transform.rotation = Rpart107;
            p108.transform.rotation = Rpart108;
            p109.transform.rotation = Rpart109;
            p110.transform.rotation = Rpart110;
            p111.transform.rotation = Rpart111;
            p112.transform.rotation = Rpart112;
            p113.transform.rotation = Rpart113;
            p114.transform.rotation = Rpart114;
            p115.transform.rotation = Rpart115;
            p116.transform.rotation = Rpart116;
            p117.transform.rotation = Rpart117;
            p118.transform.rotation = Rpart118;
            p119.transform.rotation = Rpart119;
            p120.transform.rotation = Rpart120;
            p121.transform.rotation = Rpart121;
            p122.transform.rotation = Rpart122;
            p123.transform.rotation = Rpart123;
            p124.transform.rotation = Rpart124;
            p125.transform.rotation = Rpart125;
            p126.transform.rotation = Rpart126;
            p127.transform.rotation = Rpart127;
            p128.transform.rotation = Rpart128;
            p129.transform.rotation = Rpart129;
            p130.transform.rotation = Rpart130;
            p131.transform.rotation = Rpart131;
            p132.transform.rotation = Rpart132;
            p133.transform.rotation = Rpart133;
            p134.transform.rotation = Rpart134;
            p135.transform.rotation = Rpart135;
            p136.transform.rotation = Rpart136;
            p137.transform.rotation = Rpart137;
            p138.transform.rotation = Rpart138;
            p139.transform.rotation = Rpart139;
            p140.transform.rotation = Rpart140;
            p141.transform.rotation = Rpart141;
            p142.transform.rotation = Rpart142;
            p143.transform.rotation = Rpart143;
            
        }



    }
}