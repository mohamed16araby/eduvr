using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;
public class GenricResetPosition : MonoBehaviour
{
    public  GameObject[] GameObjectParts;
    private Vector3[] initialpos;
    private Quaternion[] initialRot;

    private void Start()
    {
        initialpos = new Vector3[GameObjectParts.Length];
        initialRot = new Quaternion[GameObjectParts.Length];
        for (int i = 0; i < GameObjectParts.Length; i++) {
            initialpos[i] = GameObjectParts[i].transform.position;
            initialRot[i] = GameObjectParts[i].transform.rotation;        
        }
    }
    private void Update()
    {
        if (InputBridge.Instance.XButtonDown) {
            ResetGameOject();
        }
    }
    public void ResetGameOject() {

        for (int i = 0; i < GameObjectParts.Length; i++) {
            LeanTween.move(GameObjectParts[i], initialpos[i], 5f).setEase(LeanTweenType.linear);
            GameObjectParts[i].transform.rotation = initialRot[i];
        }

    }
}