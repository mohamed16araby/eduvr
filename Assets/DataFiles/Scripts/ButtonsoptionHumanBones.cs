﻿namespace VRTK.Examples
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using BNG;

    public class ButtonsoptionHumanBones : MonoBehaviour
    {

        public float maxAcceleration = 3f;
        public float jumpPower = 10f;

        private float acceleration = 0.05f;
        private float movementSpeed = 0f;
        private float rotationSpeed = 180f;
        private bool isJumping = false;
        private Vector2 touchAxis;
        private float triggerAxis;
        private Rigidbody rb;
        private Vector3 defaultPosition;
        private Vector3 defaulScale;
        static float step;
        private Quaternion defaultRotation;
        public float speed = 1.0f;


        /////Parts 
        public GameObject p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, 
            p23, p24, p25;
        private Vector3 ppart1, ppart2, ppart3, ppart4, ppart5, ppart6, ppart7, ppart8, ppart9, ppart10, ppart11,
            ppart12, ppart13, ppart14, ppart15, ppart16, ppart17, ppart18 ,ppart19, ppart20, ppart21, ppart22, 
            ppart23, ppart24, ppart25;
       

        private Quaternion Rpart1, Rpart2, Rpart3, Rpart4, Rpart5, Rpart6, Rpart7, Rpart8, Rpart9, Rpart10, Rpart11, 
            Rpart12, Rpart13, Rpart14, Rpart15, Rpart16, Rpart17, Rpart18, Rpart19, Rpart20, Rpart21, Rpart22, Rpart23,
            Rpart24, Rpart25;
   




        private void Awake()
        {

            // step = 10f;
            step = speed * Time.deltaTime; // calculate distance to move
                                           ///////PARTS POSATION
            ppart1 = p1.transform.position;
            ppart2 = p2.transform.position;
            ppart3 = p3.transform.position;
            ppart4 = p4.transform.position;
            ppart5 = p5.transform.position;
            ppart6 = p6.transform.position;
            ppart7 = p7.transform.position;
            ppart8 = p8.transform.position;
            ppart9 = p9.transform.position;
            ppart10 = p10.transform.position;
            ppart11 = p11.transform.position;
            ppart12 = p12.transform.position;
            ppart13 = p13.transform.position;
            ppart14 = p14.transform.position;
            ppart15 = p15.transform.position;
            ppart16 = p16.transform.position;
            ppart17 = p17.transform.position;
            ppart18 = p18.transform.position;
            ppart19 = p19.transform.position;
            ppart20 = p20.transform.position;
            ppart21 = p21.transform.position;
            ppart22 = p22.transform.position;
            ppart23 = p23.transform.position;
            ppart24 = p24.transform.position;
            ppart25 = p25.transform.position;
           
            
            
      

            Rpart1 = p1.transform.rotation;
            Rpart2 = p2.transform.rotation;
            Rpart3 = p3.transform.rotation;
            Rpart4 = p4.transform.rotation;
            Rpart5 = p5.transform.rotation;
            Rpart6 = p6.transform.rotation;
            Rpart7 = p7.transform.rotation;
            Rpart8 = p8.transform.rotation;
            Rpart9 = p9.transform.rotation;
            Rpart10 = p10.transform.rotation;
            Rpart11 = p11.transform.rotation;
            Rpart12 = p12.transform.rotation;
            Rpart13 = p13.transform.rotation;
            Rpart14 = p14.transform.rotation;
            Rpart15 = p15.transform.rotation;
            Rpart16 = p16.transform.rotation;
            Rpart17 = p17.transform.rotation;
            Rpart18 = p18.transform.rotation;
            Rpart19 = p19.transform.rotation;
            Rpart20 = p20.transform.rotation;
            Rpart21 = p21.transform.rotation;
            Rpart22 = p22.transform.rotation;
            Rpart23 = p23.transform.rotation;
            Rpart24 = p24.transform.rotation;
            Rpart25 = p25.transform.rotation;
           

        }
        
        private void Update()
            {
                if (InputBridge.Instance.XButtonDown)
                {
                    Resettheitems();
                }
            }
            
        public void Resettheitems()
        {

            LeanTween.move(p1, ppart1, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p2, ppart2, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p3, ppart3, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p4, ppart4, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p5, ppart5, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p6, ppart6, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p7, ppart7, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p8, ppart8, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p9, ppart9, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p10, ppart10, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p11, ppart11, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p12, ppart12, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p13, ppart13, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p14, ppart14, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p15, ppart15, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p16, ppart16, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p17, ppart17, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p18, ppart18, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p19, ppart19, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p20, ppart20, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p21, ppart21, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p22, ppart22, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p23, ppart23, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p24, ppart24, 2f).setEase(LeanTweenType.linear);
            LeanTween.move(p25, ppart25, 2f).setEase(LeanTweenType.linear);
           


            p1.transform.rotation = Rpart1;
            p2.transform.rotation = Rpart2;
            p3.transform.rotation = Rpart3;
            p4.transform.rotation = Rpart4;
            p5.transform.rotation = Rpart5;
            p6.transform.rotation = Rpart6;
            p7.transform.rotation = Rpart7;
            p8.transform.rotation = Rpart8;
            p9.transform.rotation = Rpart9;
            p10.transform.rotation = Rpart10;
            p11.transform.rotation = Rpart11;
            p12.transform.rotation = Rpart12;
            p13.transform.rotation = Rpart13;
            p14.transform.rotation = Rpart14;
            p15.transform.rotation = Rpart15;
            p16.transform.rotation = Rpart16;
            p17.transform.rotation = Rpart17;
            p18.transform.rotation = Rpart18;
            p19.transform.rotation = Rpart19;
            p20.transform.rotation = Rpart20;
            p21.transform.rotation = Rpart21;
            p22.transform.rotation = Rpart22;
            p23.transform.rotation = Rpart23;
            p24.transform.rotation = Rpart24;
            p25.transform.rotation = Rpart25;
          
        }



    }
}