using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BNG;

public class MenuManager : MonoBehaviour
{

    public void OpenScene(int id)
    {
        SceneManager.LoadScene(id);
    }

    private void Update()
    {
        if (InputBridge.Instance.AButtonDown)
        {
            SceneManager.LoadScene(0);
        }
    }
}
