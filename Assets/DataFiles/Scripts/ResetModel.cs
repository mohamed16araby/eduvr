using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetModel : MonoBehaviour
{
    public Vector3 originalPos;
    public Quaternion originalRot;

    private void Start()
    {
        originalPos = gameObject.transform.localPosition;
        originalRot = gameObject.transform.localRotation;
    }

    public void ResetPosition_Rotation()
    {
        gameObject.transform.localPosition = originalPos;
        gameObject.transform.localRotation = originalRot;
    }
}