using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;
using LiquidVolumeFX;

public class PlantDiagramManager : MonoBehaviour
{
    public GameObject snapSeed,seed,slotSeed, waterCan, liquid, root, stem, leaves1, leaves2, leaves3, flower;
    public AudioSource dataNarrationSource, replayEffect;
    public GameObject[] diagramInfoAndData;

    private void Update()
    {
        if (liquid.GetComponent<LiquidVolume>().level == 0 &&
            liquid.transform.GetChild(0).GetComponent<LiquidVolume>().level == 0)
        {
            slotSeed.SetActive(false);
            waterCan.GetComponent<ResetModel>().ResetPosition_Rotation();
            waterCan.GetComponent<BoxCollider>().enabled = false;
            root.SetActive(true);

            if (root.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            {
                stem.SetActive(true);
            }

            if (stem.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            {
                leaves1.SetActive(true);
                leaves2.SetActive(true);
                leaves3.SetActive(true);
            }

            if (leaves1.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            {
                flower.SetActive(true);
                root.GetComponent<BoxCollider>().enabled = true;
                stem.GetComponent<BoxCollider>().enabled = true;
                leaves1.GetComponent<BoxCollider>().enabled = true;
                leaves2.GetComponent<BoxCollider>().enabled = true;
                leaves3.transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
                leaves3.transform.GetChild(1).GetComponent<BoxCollider>().enabled = true;
                leaves3.transform.GetChild(2).GetComponent<BoxCollider>().enabled = true;
                flower.GetComponent<BoxCollider>().enabled = true;
            }
        }
    }

    public void RemoveItemSlotSeed()
    {
        slotSeed.GetComponent<SnapZone>().HeldItem = null;
        waterCan.SetActive(true);
    }

    public void ShowInformation(GameObject info)
    {
        info.SetActive(true);
    }

    public void PlayNarration(AudioClip clip)
    {
        dataNarrationSource.clip = clip;
        dataNarrationSource.Play();
    }

    public void ReplayDiagram()
    {
        replayEffect.Play();
        seed.transform.SetParent(snapSeed.transform);
        seed.GetComponent<ResetModel>().ResetPosition_Rotation();
        seed.GetComponent<SphereCollider>().enabled=true;
        seed.GetComponent<Grabbable>().enabled=true;
        slotSeed.SetActive(true);
        slotSeed.GetComponent<SnapZone>().HeldItem = null;
        waterCan.SetActive(false);
        waterCan.GetComponent<BoxCollider>().enabled = true;
        liquid.GetComponent<LiquidVolume>().level = 1;
        liquid.transform.GetChild(0).GetComponent<LiquidVolume>().level = 1;
        dataNarrationSource.clip = null;
        for (int i = 0; i < diagramInfoAndData.Length; i++)
        {
            diagramInfoAndData[i].SetActive(false);
        }
        root.GetComponent<BoxCollider>().enabled = false;
        stem.GetComponent<BoxCollider>().enabled = false;
        leaves1.GetComponent<BoxCollider>().enabled = false;
        leaves2.GetComponent<BoxCollider>().enabled = false;
        leaves3.transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
        leaves3.transform.GetChild(1).GetComponent<BoxCollider>().enabled = false;
        leaves3.transform.GetChild(2).GetComponent<BoxCollider>().enabled = false;
        flower.GetComponent<BoxCollider>().enabled = true;
        root.SetActive(false);
        stem.SetActive(false);
        leaves1.SetActive(false);
        leaves2.SetActive(false);
        leaves3.SetActive(false);
        flower.SetActive(false);
    }
}