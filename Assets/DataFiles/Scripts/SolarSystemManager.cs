using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarSystemManager : MonoBehaviour
{

    [SerializeField] private AudioSource narrationSource;

    public void PlayNarration(AudioClip clip)
    {
        narrationSource.clip = clip;
        narrationSource.Play();
    }
}
