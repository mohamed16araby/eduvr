using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class ResetPartsAnimated : MonoBehaviour
{
    /////Parts 
    public GameObject p1, p2, p3, p4;
    private Vector3 ppart1, ppart2, ppart3, ppart4;
    private Quaternion Rpart1, Rpart2, Rpart3, Rpart4;
    
    
    private void Awake()
    {
        ppart1 = p1.transform.position;
        ppart2 = p2.transform.position;
        ppart3 = p3.transform.position;
        ppart4 = p4.transform.position;

        Rpart1 = p1.transform.rotation;
        Rpart2 = p2.transform.rotation;
        Rpart3 = p3.transform.rotation;
        Rpart4 = p4.transform.rotation;

    }

    private void Update()
    {
        if (InputBridge.Instance.XButtonDown)
        {
            Resettheitems();
        }
    }

    public void Resettheitems()
    {

        LeanTween.move(p1, ppart1, 2f).setEase(LeanTweenType.linear);
        LeanTween.move(p2, ppart2, 2f).setEase(LeanTweenType.linear);
        LeanTween.move(p3, ppart3, 2f).setEase(LeanTweenType.linear);
        LeanTween.move(p4, ppart4, 2f).setEase(LeanTweenType.linear);

        p1.transform.rotation = Rpart1;
        p2.transform.rotation = Rpart2;
        p3.transform.rotation = Rpart3;
        p4.transform.rotation = Rpart4;

    }
}
