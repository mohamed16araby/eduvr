using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class RemoveItemFromSlot : MonoBehaviour
{
    public GameObject slotMoamen,slotAli;

    public void RemoveItemSlotMoamen()
    {
        slotMoamen.GetComponent<SnapZone>().HeldItem.GetComponent<Rigidbody>().useGravity = true;
        slotMoamen.GetComponent<SnapZone>().HeldItem.GetComponent<Rigidbody>().isKinematic = false;
        slotMoamen.GetComponent<SnapZone>().HeldItem.GetComponent<BoxCollider>().enabled = true;
        slotMoamen.GetComponent<SnapZone>().HeldItem = null;
    }
    
    public void RemoveItemSlotAli()
    {
        slotAli.GetComponent<SnapZone>().HeldItem = null;
    }
}
